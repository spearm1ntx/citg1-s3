package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>"
				+ "To use the app, input two numbers and an operation.<br>"
				+ "Hit the submit button after filling in the details<br>"
				+ "You will get the result in your browser");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
			float num1= Float.parseFloat(req.getParameter("num1"));
			float num2= Float.parseFloat(req.getParameter("num2"));
			String operation= req.getParameter("operation");
			PrintWriter out = res.getWriter();
			float result=0;
			switch(operation){
				case "add":
					result=num1+num2;
					break;
				case "subtract":
					result=num1-num2;
					break;
				case "multiply":
					result=num1*num2;
					break;
				case "divide":
					if(num2!=0) {
					result=num1/num2;}
					break;
				default:
					
			}
			out.println("The two numbers you provided are: "+(int)num1+","+(int)num2+"\n\n"
					+"The operation that you wanted is:"+operation+"\n\n"
					+"The result is: "+result +"\n");
	}
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" Information has been destoryed. ");
		System.out.println("******************************************");
	}

}
