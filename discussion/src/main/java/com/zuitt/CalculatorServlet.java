package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	/*
	 * init method is used to perform functions such as connect to databases and initalizing values to be used 
	 * in the servlet context.
	 * -loads servlet
	 * -creates an instance of the servlet
	 * -initializes the servlet instance by calling
	 * the init method.
	 * */
	public void init() throws ServletException{
		System.out.println("************");
		System.out.println("Initialized connection to database.");
		System.out.println("************");
		
	}
	//method name "doPost" ensures that this will only be accessible via post
	//service methods(doPst, sevice)- the second stage of the servlet life cycle the handles the r
	//request and response
	//service method handles all the requests(post, get, put, delete)
	//not recommended to use service()
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws IOException {
		
		/* System.out.println("Hello from calculator servlet."); */
		/*
		 * parameter names are defined in the form input field
		 * The parameters are found in the url as query string
		 * (example: ?num1=&num2)
		 * 
		 * */
		int num1 =Integer.parseInt(req.getParameter("num1"));
		int num2 =Integer.parseInt(req.getParameter("num2"));
		int sum= num1+num2;
		//getwriter() method is used to print out info in the browser as a response
		
		PrintWriter out = res.getWriter();
		
		//this code below prints a string in the browser
		//you can also print html tags 
		out.println("<h1>the total of the 2 numbers are "+sum+"</h1>");
	}
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws IOException {
		PrintWriter out=res.getWriter();
		out.println("<h1>You have accessed the GET"
				+ " method of the calulator"
				+ "servlet</h1>");
		
	}
	/*
	 * Finalization- last part of the servlet life cycle
	 * that invokes the "destroy" method.
	 * -Clean up of the resources once the servlet is destoyed/unused
	 * -closes connection
	 * */
	public void destroy() {
		System.out.println("************");
		System.out.println("Disconnected from database.");
		System.out.println("************");
	}

}
